package com.zhuiyun.spider.service.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import com.google.common.collect.Maps;
import com.zhuiyun.common.annotation.DataScope;
import com.zhuiyun.common.core.domain.AjaxResult;
import com.zhuiyun.common.core.domain.ICallBack;
import com.zhuiyun.common.utils.DateUtils;
import com.zhuiyun.framework.util.ShiroUtils;
import com.zhuiyun.spider.backend.SpiderBackendService;
import com.zhuiyun.spider.config.SpiderConstants;
import com.zhuiyun.spider.mapper.SpiderConfigMapper;
import com.zhuiyun.spider.mapper.SpiderFieldMapper;
import com.zhuiyun.system.domain.SysUser;
import com.zhuiyun.spider.mapper.SpiderMissionMapper;
import com.zhuiyun.spider.service.ISpiderMissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.zhuiyun.spider.domain.SpiderMission;
import com.zhuiyun.common.core.text.Convert;

/**
 * 爬虫任务Service业务层处理
 *
 * @author wujiyue
 * @date 2019-11-11
 */
@Service
public class SpiderMissionServiceImpl implements ISpiderMissionService
{
    @Autowired
    private SpiderMissionMapper spiderMissionMapper;
    @Autowired
    private SpiderConfigMapper spiderConfigMapper;

    @Autowired
    private SpiderFieldMapper spiderFieldMapper;

    /**
     * 查询爬虫任务
     *
     * @param missionId 爬虫任务ID
     * @return 爬虫任务
     */
    @Override
    public SpiderMission selectSpiderMissionById(Long missionId)
    {
        return spiderMissionMapper.selectSpiderMissionById(missionId);
    }

    /**
     * 查询爬虫任务列表
     *
     * @param spiderMission 爬虫任务
     * @return 爬虫任务
     */
    @Override
    @DataScope(deptAlias = "a",userAlias = "a")
    public List<SpiderMission> selectSpiderMissionList(SpiderMission spiderMission)
    {
        return spiderMissionMapper.selectSpiderMissionList(spiderMission);
    }

    /**
     * 新增爬虫任务
     *
     * @param spiderMission 爬虫任务
     * @return 结果
     */
    @Override
    public int insertSpiderMission(SpiderMission spiderMission)
    {
        SysUser user= ShiroUtils.getSysUser();
        spiderMission.setUserId(user.getUserId().toString());
        spiderMission.setDeptId(user.getDeptId().toString());
        spiderMission.setStatus(SpiderConstants.SPIDER_MISSION_STATUS_WAIT);
        spiderMission.setCreateBy(user.getUserName());
        spiderMission.setCreateTime(DateUtils.getNowDate());
        return spiderMissionMapper.insertSpiderMission(spiderMission);
    }

    /**
     * 修改爬虫任务
     *
     * @param spiderMission 爬虫任务
     * @return 结果
     */
    @Override
    public int updateSpiderMission(SpiderMission spiderMission)
    {
        return spiderMissionMapper.updateSpiderMission(spiderMission);
    }

    /**
     * 删除爬虫任务对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSpiderMissionByIds(String ids)
    {
        return spiderMissionMapper.deleteSpiderMissionByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除爬虫任务信息
     *
     * @param missionId 爬虫任务ID
     * @return 结果
     */
    @Override
    public int deleteSpiderMissionById(Long missionId)
    {
        return spiderMissionMapper.deleteSpiderMissionById(missionId);
    }

    @Override
    public AjaxResult runSpiderMission(String missionId) {
        SpiderMission mission=this.selectSpiderMissionById(Long.valueOf(missionId));

        if(mission!=null){
            if(SpiderConstants.SPIDER_MISSION_STATUS_RUNNING.equals(mission.getStatus())){
                return AjaxResult.error("该任务正在运行中!");
            }
            SpiderCallBack spiderCallBack= new SpiderCallBack();
            SpiderBackendService spiderBackendService=new SpiderBackendService(missionId,spiderCallBack);
            spiderBackendService.start();
        }
        return AjaxResult.success();
    }

    public class SpiderCallBack implements ICallBack{
        Map params= Maps.newConcurrentMap();
        @Override
        public void onSuccess() {
            System.out.println(">>>>>>>>>>>>>done>>>>>>>>>>>>>>");
            CopyOnWriteArrayList<LinkedHashMap<String, String>> datas=(CopyOnWriteArrayList<LinkedHashMap<String, String>>)params.get("datas");
            System.out.println(">>>>>>>>>>>>>"+datas.size()+">>>>>>>>>>>>>>");
        }

        @Override
        public void onFail() {

        }

        @Override
        public Map setParams(Map map) {
            params.clear();
            params.putAll(map);
            return params;
        }
    }
}
