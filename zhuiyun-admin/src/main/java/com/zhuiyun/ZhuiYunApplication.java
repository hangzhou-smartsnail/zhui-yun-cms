package com.zhuiyun;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 启动程序
 *
 * @author ruoyi
 */
@Slf4j
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class ZhuiYunApplication
{
    public static void main(String[] args) throws UnknownHostException
    {
        ConfigurableApplicationContext configurableApplicationContext = new SpringApplicationBuilder()
                .sources(ZhuiYunApplication.class).logStartupInfo(false).run(args);
        Environment env = configurableApplicationContext.getEnvironment();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        System.out.println("\n----------------------------------------------------------\n\t" +
                "Application  is running! Access URLs:\n\t" +
                "网站首页地址: \thttp://localhost:" + port + path + "\n\t" +
                "网站后台地址 \thttp://localhost:"+ port + path + "admin"+"\n\t" +
                "----------------------------------------------------------");
    }
}
