package com.zhuiyun.system.mapper;

import java.util.List;
import java.util.Map;

public interface HighSchoolMapper {
    public List<Map<String,Object>> selectSchoolLikeWords(String words);
}
