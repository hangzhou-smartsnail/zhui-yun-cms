package com.zhuiyun.system.service;

import java.util.Map;


/**
 * 省份相关业务层service
 *
 * @author feilong
 */
public interface ProvinceService {

    /****
     * 获取所在地省份相关列表
     * @param selectId  选中区域id
     * @param nowProvinceId 省份id     * @return
     */
    public Map<String,Object> getPositionProvinceList(String selectId, String nowProvinceId);

    /***
     * 根据所在地省份id获取所在地城市列表
     * @param selectId  选中区域id
     * @param provinceId 省份id
     * @param nowCityId 城市id
     * @return
     */
    public Map<String,Object> getPositionCityByProvinceId(String selectId,String provinceId,String nowCityId);

    public Map<String,Object> getPositionCountryByCityId(String selectId,String cityId,String nowCountryId);

    public Map<String,Object> getPositionTownByCountryId(String selectId,String countryId,String nowTownId);

    public Map<String,Object> initPositionSelect(String provinceId,String provinceSelectId,String cityId,String citySelectId,String countryId,String countrySelectId,String townId,String townSelectId);
}
