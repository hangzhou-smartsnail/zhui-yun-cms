package com.zhuiyun.system.service;

import java.util.List;
import java.util.Map;

/***
 * 各高校信息相关service
 * @author 飞龙
 */
public interface HighSchoolService {

    /***
     * 根据关键词模糊搜索相关高校信息
     * @param words 关键词
     * @return 高校信息列表
     */
    public List<Map<String,Object>> selectSchoolLikeWords(String words);
}
