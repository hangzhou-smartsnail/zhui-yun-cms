package com.zhuiyun.system.service.impl;

import com.zhuiyun.system.mapper.HighSchoolMapper;
import com.zhuiyun.system.service.HighSchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


/***
 * 高校相关service
 */
@Service
public class HighSchoolServiceImpl implements HighSchoolService {

    @Autowired
    HighSchoolMapper highSchoolMapper;


    /***
     * 根据关键词模糊搜索相关高校信息
     * @param words 关键词
     * @return 高校信息列表
     */
    @Override
    public List<Map<String,Object>> selectSchoolLikeWords(String words){
        return  highSchoolMapper.selectSchoolLikeWords(words);
    }
}
