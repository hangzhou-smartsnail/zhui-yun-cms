package com.zhuiyun.plugs.oss.mapper;

import com.zhuiyun.common.core.dao.BaseMapper;
import com.zhuiyun.plugs.oss.domain.SysOss;

/**
 * 文件上传
 */
public interface SysOssMapper extends BaseMapper<SysOss>
{
}
