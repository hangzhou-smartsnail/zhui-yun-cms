package com.zhuiyun.pay.wxpay.enums;

/**
 *
 * <p>微信域名枚举接口</p>
 *
 * @author 飞龙
 */
public interface WxDomainEnum {
    /**
     * 获取域名
     *
     * @return 返回域名
     */
    String getDomain();
}
