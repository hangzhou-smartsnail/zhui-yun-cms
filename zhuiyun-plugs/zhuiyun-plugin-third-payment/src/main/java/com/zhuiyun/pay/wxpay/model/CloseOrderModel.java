package com.zhuiyun.pay.wxpay.model;

import com.zhuiyun.pay.core.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * <p>关闭订单 Model</p>
 *
 * @author 飞龙
 */
@Builder
@AllArgsConstructor
@Getter
@Setter
public class CloseOrderModel extends BaseModel {
    private String appid;
    private String sub_appid;
    private String mch_id;
    private String sub_mch_id;
    private String out_trade_no;
    private String nonce_str;
    private String sign;
    private String sign_type;
}
