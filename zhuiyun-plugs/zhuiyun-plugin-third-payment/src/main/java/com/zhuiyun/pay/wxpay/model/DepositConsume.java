package com.zhuiyun.pay.wxpay.model;

import com.zhuiyun.pay.core.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * <p>消费押金 Model</p>
 *
 * @author Javen
 */
@Builder
@AllArgsConstructor
@Getter
@Setter
public class DepositConsume extends BaseModel {
    private String appid;
    private String sub_appid;
    private String mch_id;
    private String sub_mch_id;
    private String transaction_id;
    private String out_trade_no;
    private String total_fee;
    private String consume_fee;
    private String fee_type;
    private String nonce_str;
    private String sign;
    private String sign_type;
}
