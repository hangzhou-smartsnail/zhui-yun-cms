package com.zhuiyun.pay.core.enums;


/**
 *
 * <p>商户平台模式</p>
 *
 * @author 飞龙
 */
public enum PayModel {
    /**
     * 商户模式
     */
    BUSINESS_MODEL("BUSINESS_MODEL"),
    /**
     * 服务商模式
     */
    SERVICE_MODE("SERVICE_MODE");

    PayModel(String payModel) {
        this.payModel = payModel;
    }

    /**
     * 商户模式
     */
    private final String payModel;


    public String getPayModel() {
        return payModel;
    }
}
