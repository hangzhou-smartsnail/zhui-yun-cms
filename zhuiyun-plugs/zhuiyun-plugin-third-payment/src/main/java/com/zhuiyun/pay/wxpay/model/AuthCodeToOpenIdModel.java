package com.zhuiyun.pay.wxpay.model;

import com.zhuiyun.pay.core.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * <p>授权码查询 openId Model</p>
 *
 * @author 飞龙
 */
@Builder
@AllArgsConstructor
@Getter
@Setter
public class AuthCodeToOpenIdModel extends BaseModel {
    private String appid;
    private String sub_appid;
    private String mch_id;
    private String sub_mch_id;
    private String auth_code;
    private String nonce_str;
    private String sign;
    private String sign_type;

}
