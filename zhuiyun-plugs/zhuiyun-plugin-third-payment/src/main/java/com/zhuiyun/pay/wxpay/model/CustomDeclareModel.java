package com.zhuiyun.pay.wxpay.model;

import com.zhuiyun.pay.core.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Javen
 */
@Builder
@AllArgsConstructor
@Getter
@Setter
public class CustomDeclareModel extends BaseModel {
    private String sign;
    private String sign_type;
    private String appid;
    private String mch_id;
    private String out_trade_no;
    private String transaction_id;
    private String customs;
    private String mch_customs_no;
    private String duty;
    private String action_type;
    private String sub_order_no;
    private String sub_order_id;
    private String fee_type;
    private String order_fee;
    private String transport_fee;
    private String product_fee;
    private String cert_type;
    private String cert_id;
    private String name;
}
