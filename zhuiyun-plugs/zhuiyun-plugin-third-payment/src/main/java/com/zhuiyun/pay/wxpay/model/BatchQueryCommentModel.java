package com.zhuiyun.pay.wxpay.model;

import com.zhuiyun.pay.core.model.BaseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * <p>拉取订单评价数据 Model</p>
 *
 * @author 飞龙
 */
@Builder
@AllArgsConstructor
@Getter
@Setter
public class BatchQueryCommentModel extends BaseModel {
    private String appid;
    private String mch_id;
    private String nonce_str;
    private String sign;
    private String sign_type;
    private String begin_time;
    private String end_time;
    private String offset;
    private String limit;
}
