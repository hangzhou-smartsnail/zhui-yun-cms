package com.zhuiyun.plugs.thirdlogin.gitee;

import cn.hutool.http.Method;
import com.alibaba.fastjson.JSONObject;
import com.zhuiyun.common.core.domain.AjaxResult;
import com.zhuiyun.common.utils.IpUtils;
import com.zhuiyun.common.utils.ServletUtils;
import com.zhuiyun.common.utils.StringUtils;
import com.zhuiyun.framework.util.ShiroUtils;
import com.zhuiyun.plugs.thirdlogin.BaseThirdLoginController;
import com.zhuiyun.plugs.thirdlogin.domain.ThirdOauth;
import com.zhuiyun.plugs.thirdlogin.domain.ThirdPartyUser;
import com.zhuiyun.plugs.thirdlogin.util.UrlUtils;
import com.zhuiyun.system.domain.SysUser;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class GiteeThirdLoginController extends BaseThirdLoginController {

    @Value("${third.login.gitee.authorize_url}")
    public  String  AUTHORIZE_URL;
    @Value("${third.login.gitee.client_id}")
    public  String CLIENT_ID;
    @Value("${third.login.gitee.client_secret}")
    public  String CLIENT_SECRET;
    @Value("${third.login.gitee.redirect_uri}")
    public  String REDIRECT_URL;
    @Value("${third.login.gitee.scope}")
    public  String SCOPE;

    @GetMapping(value = "/thirdLogin/gitee")
    public void gitee(HttpServletRequest request, HttpServletResponse response){
        String authorizeUrl = UrlUtils.formatRequestAuthUrl(request,REDIRECT_URL,AUTHORIZE_URL,CLIENT_ID,SCOPE);
        try {
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<script>");
            out.println("window.open ('"+authorizeUrl+"','_top')");
            out.println("</script>");
            out.println("</html>");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/thirdLogin/gitee/callback")
    public String giteeCallback(HttpServletRequest request) throws Exception {
        String host = request.getHeader("host");
        String code = request.getParameter("code");
        String state = request.getParameter("state");
        String redirect = request.getParameter("redirect");
        if(StringUtils.isEmpty(code)){
            return "redirect:/thirdLogin/gitee";
        }
        //获取用户AccessToken
        AjaxResult ajaxResult = this.getAccessToken(code,host);
        if(ajaxResult.isSuccess()){
            JSONObject jsonObject=(JSONObject)ajaxResult.get("data");
            String access_token = jsonObject.getString("access_token");
            if(StringUtils.isEmpty(access_token)){
                System.out.println("==>/thirdLogin/gitee/callback=======获得access_token为空!");
                return "redirect:/thirdLogin/gitee";
            }
            //获取用户
            ThirdPartyUser thirdPartyUser = this.geAccessTokentUserInfo(access_token);
            if(thirdPartyUser!=null){
                thirdPartyUser.setAccessToken(access_token);
                //查询第三方登录
                ThirdOauth form=new ThirdOauth();
                form.setLoginType("gitee");
                form.setOpenid(thirdPartyUser.getOpenid());
                List<ThirdOauth> list=thirdOauthService.selectThirdOauthList(form);
                if(CollectionUtils.isEmpty(list)){
                    //未绑定平台用户，去引导绑定用户
                    if(StringUtils.isEmpty(BIND_URL)){
                        //如果为空，自动绑定用户
                       SysUser user = registThirdUser(thirdPartyUser);
                       insertThirdOauth(thirdPartyUser,user.getUserId().toString());
                        list=thirdOauthService.selectThirdOauthList(form);
                    }else{
                        //跳转到绑定用户页面
                        String url=BIND_URL+"?type=gitee&openid="+thirdPartyUser.getOpenid()+"&successUri="+redirect+"&thirdAccount="+thirdPartyUser.getAccount();
                        request.getSession().setAttribute("access_token",access_token);
                        return "redirect:"+url;
                    }
                }
                //已经绑定过
                //登录
                ThirdOauth thirdOauth=list.get(0);
                String userId=thirdOauth.getUserId();//本平台的用户ID
                AjaxResult ajaxResult1 = thirdLoginService.loginNoPwd(Long.valueOf(userId));
                if(ajaxResult1.isSuccess()){
                    //返回
                    if(StringUtils.isEmpty(redirect)){
                        return "redirect:/";
                    }else{
                        return "redirect:"+redirect;
                    }
                }else{
                    return "redirect:/thirdLogin/gitee";
                }
            }
            System.out.println("==>/thirdLogin/gitee/callback=======获得ThirdPartyUser为空!");
            return "redirect:/thirdLogin/gitee";
        }
        return "redirect:/thirdLogin/gitee";
    }

    private SysUser registThirdUser(ThirdPartyUser thirdPartyUser){
        HttpServletRequest request= ServletUtils.getRequest();
        SysUser user=new SysUser();
        user.setLoginName(thirdPartyUser.getAccount());
        user.setUserName(thirdPartyUser.getUserName());
        user.setSalt(ShiroUtils.randomSalt());
        user.setPassword(passwordService.encryptPassword(user.getLoginName(), "88888888", user.getSalt()));
        user.setLoginIp(IpUtils.getIpAddr(request));
        user.setAvatar(thirdPartyUser.getAvatarUrl());
        user.setSex("1");
        user.setStatus("0");
        user.setDelFlag("0");
        Long[] roleIds={Long.valueOf(registerUserRoleId)};//前台用户注册赋予注册用户的角色和部门
        user.setRoleIds(roleIds);
        user.setDeptId(Long.valueOf(registerUserDeptId));
        int n = userService.insertUser(user);
        if(n>0){
            String invite_user_id=request.getParameter("invite_user_id");
            if(StringUtils.isNotEmpty(invite_user_id)){
                String ip= IpUtils.getIpAddr(request);
                userService.insertUserInvite(user.getLoginName(),invite_user_id,ip);//插入用户邀请记录表
            }
            return  user;
        }else{
            return  null;
        }
    }

    /**
     * 根据用户授权code获取授权token
     */
    private AjaxResult getAccessToken(String code, String host) throws Exception {
        String redirectUrl="http://"+host+REDIRECT_URL;
        Map<String,Object> authParams = new HashMap<>(5);
        authParams.put("grant_type" , "authorization_code");
        authParams.put("client_id" , CLIENT_ID);
        authParams.put("client_secret" , CLIENT_SECRET);
        authParams.put("code" , code);
        authParams.put("redirect_uri",redirectUrl);
        try {
            String url = "https://gitee.com/oauth/token";
            String result  =  UrlUtils.requestUrl(url,Method.POST,authParams);
            JSONObject jsonObject=JSONObject.parseObject(result);
            return AjaxResult.success(jsonObject);
        } catch (Exception e) {
            logger.error("gitee获取token失败!",e);
            return AjaxResult.error("gitee获取token失败!");
        }
    }




    /**
     * 根据授权token获取对应的用户详细信息
     */
    private ThirdPartyUser geAccessTokentUserInfo(String accessToken) throws Exception {
        String url = "https://gitee.com/api/v5/user?access_token=" + accessToken;
        try {
            String result =  UrlUtils.requestUrl(url, Method.GET,null);
            JSONObject jsonObject=JSONObject.parseObject(result);
            String loginAccount=jsonObject.getString("login");
            if(StringUtils.isNotEmpty(loginAccount)){
                ThirdPartyUser thirdPartyUser=new ThirdPartyUser();
                thirdPartyUser.setAccount(loginAccount);
                thirdPartyUser.setAvatarUrl(jsonObject.getString("avatar_url").contains("no_portrait.png")?"":jsonObject.getString("avatar_url"));
                thirdPartyUser.setOpenid(jsonObject.getString("id"));
                thirdPartyUser.setProvider("gitee");
                thirdPartyUser.setUserName(jsonObject.getString("name"));
                thirdPartyUser.setToken(accessToken);
                return thirdPartyUser;
            }
        } catch (Exception e) {
            logger.error("使用gitee账号进行第三方登录时授权信息获取失败!",e);
        }
        return null;
    }



    @Override
    public void bindSaveCallBack(SysUser user) {

    }
}
