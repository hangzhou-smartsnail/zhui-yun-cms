package com.zhuiyun.plugs.thirdlogin.qq;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.http.Method;
import com.alibaba.fastjson.JSONObject;
import com.zhuiyun.common.core.controller.BaseController;
import com.zhuiyun.common.core.domain.AjaxResult;
import com.zhuiyun.common.utils.DateUtils;
import com.zhuiyun.common.utils.IpUtils;
import com.zhuiyun.common.utils.ServletUtils;
import com.zhuiyun.common.utils.StringUtils;
import com.zhuiyun.framework.shiro.service.SysPasswordService;
import com.zhuiyun.framework.util.ShiroUtils;
import com.zhuiyun.plugs.thirdlogin.domain.ThirdPartyUser;
import com.zhuiyun.plugs.thirdlogin.domain.ThirdOauth;
import com.zhuiyun.plugs.thirdlogin.service.IThirdOauthService;
import com.zhuiyun.plugs.thirdlogin.service.ThirdLoginService;
import com.zhuiyun.plugs.thirdlogin.util.UrlUtils;
import com.zhuiyun.system.domain.SysUser;
import com.zhuiyun.system.service.ISysUserService;
import org.apache.commons.collections.CollectionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class QQThirdLogin extends BaseController {

    @Value("${third.login.qq.authorize_url}")
    public  String  AUTHORIZE_URL;
    @Value("${third.login.qq.client_id}")
    public  String CLIENT_ID;
    @Value("${third.login.qq.client_secret}")
    public  String CLIENT_SECRET;
    @Value("${third.login.qq.redirect_uri}")
    public  String REDIRECT_URL;
    @Value("${third.login.qq.scope}")
    public  String SCOPE;
    @Value("${third.login.to_bind_url}")
    public  String BIND_URL;
    @Autowired
    ISysUserService userService;
    @Autowired
    private SysPasswordService passwordService;

    @Value("${front.register.deptId}")
    public  String  registerUserDeptId;//前台用户注册赋予的默认部门id
    @Value("${front.register.roleId}")
    public  String registerUserRoleId;//前台用户注册赋予的默认角色id

    @Autowired
    private IThirdOauthService thirdOauthService;
    @Autowired
    private ThirdLoginService thirdLoginService;

    @GetMapping(value = "/thirdLogin/qq")
    public void qq(HttpServletRequest request, HttpServletResponse response){
        String authorizeUrl = UrlUtils.formatRequestAuthUrl(request,REDIRECT_URL,AUTHORIZE_URL,CLIENT_ID,SCOPE);
        try {
            PrintWriter out = response.getWriter();
            out.println("<html>");
            out.println("<script>");
            out.println("window.open ('"+authorizeUrl+"','_top')");
            out.println("</script>");
            out.println("</html>");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



    @RequestMapping(value = "/thirdLogin/unbind/qq")
    @ResponseBody
    public AjaxResult unbindQQ(HttpServletRequest request, HttpServletResponse response){
        SysUser user= ShiroUtils.getSysUser();
        if(user!=null){
            String userId=user.getUserId().toString();
            String type="qq";
            int n = thirdOauthService.deleteThirdOauthByUserIdAndLoginType(userId,type);
            if(n>0){
                return AjaxResult.success("解绑QQ账号成功!");
            }
        }
        return error();
    }
    @RequestMapping(value = "/thirdLogin/qq/callback")
    public String qqCallback(HttpServletRequest request) throws Exception {

        String host = request.getHeader("host");
        String code = request.getParameter("code");
        String state = request.getParameter("state");
        String redirect = request.getParameter("redirect");
        if(StringUtils.isEmpty(code)){
            return "redirect:/thirdLogin/qq";
        }
        //获取用户AccessToken
        AjaxResult ajaxResult = this.getQQTokenAndOpenid(code,host);
        if(ajaxResult.isSuccess()) {
            Map resMap = (Map) ajaxResult.get("data");
            String openid=String.valueOf(resMap.get("openid"));

            if(StringUtils.isEmpty(openid)){
                System.out.println("============QQ第三方登录未获取到openid!");
                return "redirect:/thirdLogin/qq";
            }
            //根据openid获取用户信息
            String access_token=String.valueOf(resMap.get("access_token"));
            ThirdPartyUser thirdPartyUser = this.getQQUserinfo(access_token,openid);
            if(thirdPartyUser!=null){
                //查询第三方登录
                ThirdOauth form=new ThirdOauth();
                form.setLoginType("qq");
                form.setOpenid(thirdPartyUser.getOpenid());
                List<ThirdOauth> list=thirdOauthService.selectThirdOauthList(form);
                if(CollectionUtils.isEmpty(list)){
                    //未绑定平台用户，去引导绑定用户
                    if(StringUtils.isEmpty(BIND_URL)){
                        //如果为空，自动绑定用户
                        SysUser user = registThirdUser(thirdPartyUser);
                        insertThirdOauth(thirdPartyUser,user.getUserId().toString());
                        list=thirdOauthService.selectThirdOauthList(form);
                    }else{
                        //跳转到绑定用户页面
                        return "redirect:"+BIND_URL;
                    }
                }
                //登录
                ThirdOauth thirdOauth=list.get(0);
                String userId=thirdOauth.getUserId();//本平台的用户ID
                AjaxResult ajaxResult1 = thirdLoginService.loginNoPwd(Long.valueOf(userId));
                if(ajaxResult1.isSuccess()){
                    //返回
                    if(StringUtils.isEmpty(redirect)){
                        return "redirect:/";
                    }else{
                        return "redirect:"+redirect;
                    }
                }else{
                    return "redirect:/thirdLogin/qq";
                }
            }
        }
        return "redirect:/thirdLogin/qq";
    }


    private SysUser registThirdUser(ThirdPartyUser thirdPartyUser){
        HttpServletRequest request= ServletUtils.getRequest();
        SysUser user=new SysUser();
        user.setLoginName(thirdPartyUser.getAccount());
        user.setUserName(thirdPartyUser.getUserName());
        user.setSalt(ShiroUtils.randomSalt());
        user.setPassword(passwordService.encryptPassword(user.getLoginName(), "88888888", user.getSalt()));
        user.setLoginIp(IpUtils.getIpAddr(request));
        user.setAvatar(thirdPartyUser.getAvatarUrl());
        user.setSex(thirdPartyUser.getGender());
        user.setStatus("0");
        user.setDelFlag("0");
        //前台用户注册赋予注册用户的角色和部门
        Long[] roleIds={Long.valueOf(registerUserRoleId)};
        user.setRoleIds(roleIds);
        user.setDeptId(Long.valueOf(registerUserDeptId));
        int n = userService.insertUser(user);
        if(n>0){
            String invite_user_id=request.getParameter("invite_user_id");
            if(StringUtils.isNotEmpty(invite_user_id)){
                String ip= IpUtils.getIpAddr(request);
                //插入用户邀请记录表
                userService.insertUserInvite(user.getLoginName(),invite_user_id,ip);
            }
            return  user;
        }else{
            return  null;
        }
    }
    private void insertThirdOauth(ThirdPartyUser thirdPartyUser,String userId){
        ThirdOauth thirdOauth=new ThirdOauth();
        thirdOauth.setOpenid(thirdPartyUser.getOpenid());
        thirdOauth.setLoginType(thirdPartyUser.getProvider());
        thirdOauth.setBindTime(DateUtils.getTime());
        thirdOauth.setUserId(userId);
        ThirdOauth form=new ThirdOauth();
        form.setOpenid(thirdPartyUser.getOpenid());
        form.setLoginType(thirdPartyUser.getProvider());
        form.setUserId(userId);
        List<ThirdOauth>  list = thirdOauthService.selectThirdOauthList(form);
        if(CollectionUtils.isEmpty(list)){
            thirdOauthService.insertThirdOauth(thirdOauth);
        }

    }

    private ThirdPartyUser getQQUserinfo(String token, String openid){
        ThirdPartyUser user = new ThirdPartyUser();
        user.setProvider("qq");
        user.setOpenid(openid);
        user.setToken(token);
        String userInfoUrl="https://graph.qq.com/user/get_user_info";
        Map<String,Object> params = new HashMap<>(4);
        params.put("access_token" , token);
        params.put("oauth_consumer_key" , CLIENT_ID);
        params.put("openid" , openid);
        String res=UrlUtils.requestUrl(userInfoUrl,Method.POST,params);
        JSONObject jsonObject = JSONObject.parseObject(res);
        if (StringUtils.isNotEmpty(res)&&jsonObject.getIntValue("ret")==0) {
            user.setUserName(jsonObject.getString("nickname"));
            String img = jsonObject.getString("figureurl_qq_2");
            if (img == null || "".equals(img)) {
                img = jsonObject.getString("figureurl_qq_1");
            }
            user.setAvatarUrl(img);
            String sex = jsonObject.getString("gender");
            if ("女".equals(sex)) {
                user.setGender("0");
            } else {
                user.setGender("1");
            }
            return user;
        }
        return null;
    }


    /***
     * 根据授权码
     * @param code
     * @param host
     * @return
     * @throws Exception
     */
    private AjaxResult getQQTokenAndOpenid(String code, String host) throws Exception {

        Map<String, String> map = new HashMap<String, String>();

        String redirectUrl="http://"+host+REDIRECT_URL;
        Map<String,Object> authParams = new HashMap<>(5);
        authParams.put("grant_type" , "authorization_code");
        authParams.put("client_id" , CLIENT_ID);
        authParams.put("client_secret" , CLIENT_SECRET);
        authParams.put("code" , code);
        authParams.put("redirect_uri",redirectUrl);
        String url = "https://graph.qq.com/oauth2.0/token";
        String result = UrlUtils.requestUrl(url, Method.POST,authParams);
        if(StringUtils.isNotEmpty(result)&&result.contains("access_token")){
            JSONObject jsonObject=JSONObject.parseObject(result);
            String openIdUrl ="https://graph.qq.com/oauth2.0/me";
            openIdUrl = openIdUrl + "?access_token=" + jsonObject.getString("access_token");
            Map<String,Object> openParams = new HashMap<>(1);
            openParams.put("access_token" , jsonObject.getString("access_token"));
            result=UrlUtils.requestUrl(openIdUrl,Method.POST,openParams);
            if(result.contains("(") && result.contains(")")){
                int i = result.indexOf("(");
                int j = result.indexOf(")");
                result = result.substring(i + 1, j);
            }
            JSONObject openidObj = JSONObject.parseObject(result);
            map.put("access_token",jsonObject.getString("access_token"));
            map.put("openid",openidObj.getString("openid"));
            return AjaxResult.success(map);
        }
        return AjaxResult.error("qq获取token失败!");
    }




}
