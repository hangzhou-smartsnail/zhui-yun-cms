package com.zhuiyun.forum.aspect;

import com.zhuiyun.common.exception.BusinessException;
import com.zhuiyun.forum.domain.ForumQuestion;
import com.zhuiyun.forum.service.IForumQuestionService;
import com.zhuiyun.framework.util.ShiroUtils;
import com.zhuiyun.system.domain.SysUser;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class QuestionDetailAspect {
    @Autowired
    IForumQuestionService forumQuestionService;
    /**
     * 帖子详情
     */
    @Pointcut("execution(* com.zhuiyun.forum.controller.ForumController.questionDetail(..))")
    public void questionDetail() {
    }
    private static final int HIDE_TYPE_LOGIN=1;
    @Before("questionDetail()")
    public void doBefore(JoinPoint point) throws Throwable
    {
        Object[] params =  point.getArgs();
        String id=(String)params[0];
        ForumQuestion forumQuestion = forumQuestionService.selectForumQuestionById(id);
        int n=forumQuestion.getHideType();
        SysUser user= ShiroUtils.getSysUser();
        if(user==null&&n==HIDE_TYPE_LOGIN){
            throw new BusinessException("请登陆后再浏览该帖子!");
        }
    }
}
