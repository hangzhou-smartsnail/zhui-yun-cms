package com.zhuiyun.forum.job;

import com.zhuiyun.forum.service.ForumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 方便手工清除前台缓存
 */
@Component("clearForumCacheJob")
public class ClearForumCacheJob {

    @Autowired
    ForumService forumService;

    public void clearCache()
    {
        System.out.println("==========触发清空论坛缓存===========");
        forumService.clearCache();
    }
}
