## 平台简介

本平台为CMS信息化平台,团队将专门为内蒙古地区中小微企业以及相关有需求的用户提供长期技术解决方案

## 作者联系方式

13651213593(微信同手机号) 作者来自内蒙古满洲里,加本人微信可直接进入微信群

## 追云CMS系统线上项目案例git地址
  敖汉找房微信小程序: https://gitee.com/feiLg/aoHanZhaoFang.git

## qq群
追云CMS交流qq群号:1044465333,加群可直接获取数据库文件,本系统部分模块尚处于开发阶段,欢迎大家参与并完善

## 追云CMS内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

## 追云cms系统即将新开发的功能

1. 新增适用于内蒙古地区中小微企业定制化电子合同业务系统。
2. 完善并根据实际需求场景拓展原有博客,内容管理业务模块中部分功能。
3. 调整原有附件管理模块,新增基于WEB端的网盘管理功能以满足内蒙古中小微企业或个人需求需要。
4. 为满足满洲里市各木材企业日益增长的数据刚需,本系统将为此打造一个数据共享资源池配套功能,使各位木材人们可方便,快捷,免费第一时间获取到自己所需的数据。
5. 为满足用户研究领域,企业日常经营活动中流程精细化管理等其他领域相关审批所需,新增基础流程审批相关业务功能。
6. 针对木材领域交易场景相关需求进行二次创新改造,新增直播看货功能

## 追云CMS系统各模块概述
   
<div>
   <img src="https://gitee.com/feiLg/zhui-yun-cms/raw/develop/uploads/2020/09/17/cms-develop-progress.png "/>
</div>
## 追云CMS新开发模块功能设计图:
 <table>
    <tr>
        <td>国内某某企业官网构建思路</td>
        <td><img src="https://gitee.com/feiLg/zhui-yun-cms/raw/develop/uploads/2023/企业官网构建思路.png"/></td>
    </tr>
    <tr>
        <td>电子合同业务系统各功能模块设计</td>
        <td><img src="https://gitee.com/feiLg/zhui-yun-cms/raw/develop/uploads/2020/09/17/psc.png "/></td>
    </tr>
     <tr>
        <td>电子合同业务移动端UI设计</td>
        <td> 
           <img src="https://gitee.com/feiLg/zhui-yun-cms/raw/develop/uploads/2020/09/17/79f0f736afc379310d1debb44159474d42a9115f.jpeg "/>
           <img src="https://gitee.com/feiLg/zhui-yun-cms/raw/develop/uploads/2020/09/17/Dingtalk_20220718174144.jpg "/>
        </td>
    </tr>
     <tr>
        <td>企业云盘系统ui设计图</td>
        <td><img src="https://gitee.com/feiLg/zhui-yun-cms/raw/develop/uploads/2020/10/06/dzqz.jpg"/></td>
    </tr>
</table>


## 追云CMS基础功能模块演示图:
<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090811_47d8563a_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/090954_1cf23ae8_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091030_92471a5c_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091108_8d470b37_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091141_ac064647_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/091415_f92464ed_528854.png "/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092642_d112390c_528854.png "/></td>
        <td><img src="https://images.gitee.com/uploads/images/2019/1204/092730_196ee826_528854.png "/></td>
    </tr>
</table>



